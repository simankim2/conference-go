import json
import requests
from .keys import PEXELS_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
